import Data.List

data BinTree a 	= 	Empty | Node a (BinTree a) (BinTree a) 
										deriving (Eq, Ord)
		
		
instance (Show a) => Show (BinTree a) where
	show t = printLevel 0 $ offsets 1 t
		where 		
			offsets pos (Empty) = []
			offsets pos (Node x l r) = offsets (pos * 2) l ++ [(pos, x)] ++ offsets (pos * 2 + 1) r
			
			printLevel lvl [] = []
			printLevel lvl xs = printNodes cur_xs ++ "\n" ++ printLevel (lvl + 1) rem_xs 	
				where 
					(cur_xs, rem_xs) = partition on_level xs
						where
							on_level (x, _) | x >= 2 ^ lvl, x < 2 ^ (lvl + 1) = True
											| otherwise = False
										
					printNodes xs = foldl addNode [] xs
					addNode str (x, y) = str ++ spaces ++ "[" ++ show y ++ "]"
						where							
							spaces = take (offset - length str) $ repeat ' '
								where 									
									beg = 64 `div` 2 ^ (lvl + 1)
									offset = (1 + 2 * (x - 2 ^ lvl)) * beg 
																				
fromList :: (Ord a) => [a] -> BinTree a
fromList [] = Empty
fromList (x:xs) = Node x (fromList (filter (<x) xs)) (fromList (filter (>x) xs))
					 
hasElement :: (Ord a) => a -> BinTree a -> Bool
hasElement _ Empty = False
hasElement elem (Node x l r)  	| elem < x 	= hasElement elem l
								| elem > x 	= hasElement elem r
								| otherwise = True

insertElement :: (Ord a) => a -> BinTree a -> BinTree a
insertElement x Empty = Node x Empty Empty
insertElement x n@(Node v l r) 	| x < v 	= Node v (insertElement x l) r
								| x > v 	= Node v l (insertElement x r)
								| otherwise = n
								
removeElement :: (Ord a) => a -> BinTree a -> BinTree a
removeElement elem Empty = Empty
removeElement elem n@(Node x l r) 	| elem < x 	= Node x (removeElement elem l) r
									| elem > x 	= Node x l (removeElement elem r)
									| otherwise = removeCurrent n
	where	
		removeCurrent (Node _ l Empty) = l
		removeCurrent (Node _ Empty r) = r
		removeCurrent (Node _ l r) = Node newVal l newRight
			where
				(newVal, newRight) = deleteLeft r
					where
						deleteLeft (Node v Empty r) = (v, r)
						deleteLeft (Node v l r) = (v', Node v l' r)
							where 
								(v', l') = deleteLeft l
		
		
										

--main = print $ fromList [4,2,1,3,6,5,7]		 
--main = print $ insertElement 3 $ fromList [4,2,1,6,5,7]		 
--main = print $ hasElement 4 $ fromList [4,2,1,6,5,7]		 
main = print $ removeElement 1 $ fromList [4,2,1,3,6,5,7]	

							 